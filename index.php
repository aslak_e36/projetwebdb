<?php
/**
 * Created by PhpStorm.
 * User: Pascal.BENZONANA
 * Date: 08.05.2017
 * Time: 08:54
 * Update : 31-JAN-2019 - nicolas.glassey
 *          Simplify index. Remove all pages references.
 */

session_start();
require "controler/rent.php";
require "controler/cart.php";
require "controler/snows.php";
require "controler/users.php";

if (isset($_GET['action'])) {
  $action = $_GET['action'];
  switch ($action) {
      case 'home' :
          home();
          break;
      case 'login' :
          login($_POST);
          break;
      case 'logout' :
          logout();
          break;
      case 'register' :
          register($_POST);
          break;
      case 'displaySnows' :
          displaySnows();
          break;
      case 'displayASnow' :
          displayASnow($_GET['code']);
          break;
      case 'snowLeasingRequest':
          snowLeasingRequest($_GET['code']);
          break;
      case 'updateCartRequest':
          updateCartRequest($_GET['code'],$_POST, $_GET['update']??null, $_GET['delete']??null);
          break;
      case 'displayCart':
          displayCart();
          break;
      case 'displayRent':
          displayRent();
          break;
      case 'fixRent':
          fixRent();
          break;
      case 'displaySellerOverview':
          displaySellerOverview();
          break;
      case 'displaySellerManageLocation':
          displaySellerManageLocation($_GET['rentId']);
          break;
      case 'manageRents':
          manageRents($_GET['rentId'], $_POST??null);
          break;
      default :
          home();
  }
}
else {
    home();
}