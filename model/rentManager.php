<?php
/**
 * This php file is designed to manage all operation regarding rent's management
 * Author   : arben.ferati@cpnv.ch
 * Project  : Projet Web + DB
 * Created  : 24.05.2019 - 11:20
 */


/**
 * This function is designed to display the last rent and to insert into database the rent
 * @param $cart -> The final cart
 * @param $user -> The user who's renting
 * @return array -> Last Rent
 * @throws Exception
 */
function finishRent($cart, $user){
    require_once 'model/dbConnector.php';

    $result = array();
    $snowsToEdit = array();
    $rentDetails = array();

    //We extract the user's ID
    $q = 'SELECT id FROM users where userEmailAddress = "' . $user . '"';
    $userId = executeQuerySelect($q);

    $insertQ = 'insert into rents(startRent, users_id, statutRent) values ("' . $cart[0]['dateD'] . '","' . $userId[0][0] . '", "En cours")';
    executeQueryInsert($insertQ);

    //We select all user's rent of the day and we reverse the array to get the last one in the first place
    $q = 'select id from rents where users_id = "' . $userId[0][0] . '" and startRent = "' . $cart[0]['dateD'] . '"';
    $rentId = executeQuerySelect($q);
    $rentId = array_reverse($rentId);

    foreach ($cart as $key => $snow){
        $q = 'SELECT id, code, brand, model, dailyPrice FROM snows where code = "' . $snow['code'] . '"';
        array_push($snowsToEdit, executeQuerySelect($q));
    }

    //We update the database here
    foreach ($cart as $key => $snow){
        $q = 'update snows set qtyAvailable = qtyAvailable - ' . $cart[$key]['qty'] . ' where id = ' . $snowsToEdit[$key][0]['id'] ;
        executeQueryInsert($q);
        $date = new DateTime($cart[$key]['dateD']);
        $newDate = 'P'.$cart[$key]['nbD'].'D';
        $date->add(new DateInterval($newDate));
        $q = 'insert into rentdetails (numberSnow, numberDay, rents_id, snows_id, users_id, endDate, statut) values ("'.$snow['qty'].'","'.$snow['nbD'].'","'.$rentId[0][0].'", "'.$snowsToEdit[$key][0]['id'].'", "'.$userId[0][0].'","'.$date->format('Y-m-d').'", "En cours")';
        executeQueryInsert($q);
    }

    $rentDetails = selectQueryRentDetails($rentId[0][0]);

    //We prepare to display the result
    foreach ($rentDetails as $key => $id){
        $result[$key]['nLocation'] = $id['rents_id'];
        $result[$key]['code'] = $cart[$key]['code'];
        $result[$key]['brand'] = $snowsToEdit[$key][0]['brand'];
        $result[$key]['model'] = $snowsToEdit[$key][0]['model'];
        $result[$key]['dailyPrice'] = $snowsToEdit[$key][0]['dailyPrice'] . '.- CHF';
        $result[$key]['qtyAvailable'] = $cart[$key]['qty'];
        $date = strtotime($cart[$key]['dateD']);
        $result[$key]['dateD'] = date('d-m-Y', $date);;
    }

    return $result;
}

/**
 * This function is designed to extract all rents of the user.
 * @param $user -> User's email (need it to get the user ID)
 * @return array -> User's rents
 */
function getAllRentsFromAUser($user){
    require_once 'model/dbConnector.php';

    $result = array();
    $snowsDetails = [];
    $rent = [];

    //We extract the user ID from database
    $q = 'SELECT id FROM users where userEmailAddress = "' . $user . '"';
    $userId = executeQuerySelect($q);

    //We extract user's rents details from database
    $rentDetails = selectQueryRentDetails(null, $userId[0][0]);

    //We extract snowboard info and rents info that are linked to the user so we can use these information later to display them
    foreach ($rentDetails as $key => $item){
        $q = 'select distinct * from snows where id = ' . $rentDetails[$key]['snows_id'];
        $tmp = executeQuerySelect($q);
        array_push($snowsDetails, $tmp);
        $q = 'select distinct * from rents where id = ' . $rentDetails[$key]['rents_id'];
        $tmp = executeQuerySelect($q);
        array_push($rent, $tmp);
    }

    //We prepare the final result
    foreach ($snowsDetails as $key => $item){
        $result[$key]['nLocation'] = $rentDetails[$key]['rents_id'];
        $result[$key]['code'] = $snowsDetails[$key][0]['code'];
        $result[$key]['brand'] = $snowsDetails[$key][0]['brand'];
        $result[$key]['model'] = $snowsDetails[$key][0]['model'];
        $result[$key]['dailyPrice'] = $snowsDetails[$key][0]['dailyPrice'] . '.- CHF';
        $result[$key]['qtyAvailable'] = $rentDetails[$key]['numberSnow'];
        $date = strtotime($rent[$key][0]['startRent']);
        $result[$key]['dateD'] = date('d-m-Y', $date);
    }
    return $result;
}

/**
 * This function is designed to extract all rents from database
 * @return array -> All rents
 */
function getAllRents(){
    require_once 'model/dbConnector.php';

    $result = array();
    $userEmail = '';
    $rent = [];

    //Extracting all rents
    $q = 'select distinct * from rents';
    $rent = executeQuerySelect($q);

    foreach ($rent as $key => $item){

        //Etracting user's email address
        $q = 'select userEmailAddress from users where id = ' . $rent[$key]['users_id'];
        $userEmail = executeQuerySelect($q);

        //Extracting user's rent details ordered by descending "endDate" so we have the latest date in the first place
        $rentDetails = selectQueryRentDetails($rent[$key]['id'], $rent[$key]["users_id"], null, " order by endDate desc");

        //Preparing the result
        $result[$key]['location'] = $rent[$key]['id'];
        $result[$key]['client'] = $userEmail[0]['userEmailAddress'];
        $date = strtotime($rent[$key]['startRent']);
        $result[$key]['firstDate'] = date('d-m-Y', $date);
        $date = strtotime($rentDetails[0]['endDate']);
        $result[$key]['lastDate'] = date('d-m-Y', $date);
        $result[$key]['statut'] = checkStatut($rentDetails);
    }
    return $result;
}

/**
 * This function is designed to display rents with details
 * @param $idRent
 * @return array
 */
function displayRents($idRent){
    require_once 'model/dbConnector.php';

    $result = [];
    $rentDetails = [];
    $snow = [];
    $rent = [];

    $rentDetails = selectQueryRentDetails($idRent);

    $q = 'select * from rents where id = ' . $idRent;
    $rent = executeQuerySelect($q);

    $q = 'select userEmailAddress from users where id = '.$rent[0]['users_id'];
    $user = executeQuerySelect($q);

    foreach ($rentDetails as $key => $item){
        $q = 'select * from snows where id = ' . $item['snows_id'];
        $snow = executeQuerySelect($q);

        $result[$key]['code'] = $snow[0]['code'];
        $result[$key]['qty'] = $item['numberSnow'];
        $result[$key]['firstDate'] = $rent[0]['startRent'];
        $result[$key]['lastDate'] = $item['endDate'];
        $result[$key]['email'] = $user[0][0];
        $result[$key]['statut'] = $item['statut'];
    }
    return $result;
}

/**
 * This function is designed for updating database when snows are returned
 * @param $rentId -> the rentID that we want to update
 * @param $data -> new data
 * @return array
 */
function updateRents($rentId, $data = null){

    require_once 'model/dbConnector.php';

    $result = [];
    $rentDetails = [];

    $rentDetails = selectQueryRentDetails($rentId, null, "Rendu");

    //We update here every snow
    if ($data != null){
        foreach ($rentDetails as $key => $rentDetail) {
            if (isset($data[$key])){
                $q = 'update rentdetails set statut = "' . $data[$key] . '" where numberDay = ' . $rentDetail['numberDay'] . ' and snows_id = ' . $rentDetail['snows_id'];
                executeQueryInsert($q);
            }
            if (isset($data[$key]) and $data[$key]=="Rendu"){
                $q = 'update snows set qtyAvailable = qtyAvailable + ' . $rentDetail['numberSnow'] . ' where id = ' . $rentDetail['snows_id'];
                executeQueryInsert($q);
            }
        }
    }

    //We desplay rents
    $result = displayRents($rentId);

    return $result;
}

/**
 * This function is designed to cover all request to the DB about rent details
 * @param null $rentId -> If we need details from a rent's id
 * @param null $userId -> If we need details from a user's rents
 * @param null $status -> If we need details from a snow that is not returned
 * @param null $orderBy -> If we need to order by something
 * @return array|null -> The result of the query
 */
function selectQueryRentDetails($rentId = null, $userId = null, $status = null, $orderBy = null){
    $result = [];
    if ($status != null){
        $q = 'select * from rentdetails where rents_id = ' . $rentId . ' and statut != "' . $status . '"';
        $result = executeQuerySelect($q);
    }elseif ($userId != null and $rentId == null) {
        $q = 'select * from rentdetails where users_id = ' . $userId;
        $result = executeQuerySelect($q);
    }elseif ($userId != null and $rentId != null and $orderBy == null){
        $q = 'select * from rentdetails where users_id = "' . $userId . '" and rents_id = ' . $rentId;
        $result = executeQuerySelect($q);
    }elseif ($orderBy != null){
        $q = 'select * from rentdetails where users_id = '. $userId . ' and rents_id = ' . $rentId . $orderBy;
        $result = executeQuerySelect($q);
    }else{
        $q = 'select * from rentdetails where rents_id = ' . $rentId;
        $result = executeQuerySelect($q);
    }
    return $result;
}

/**
 * This fucntion is designed to check status
 * @param $data
 * @return string
 */
function checkStatut($data){
    $result = '';
    $statut = 0;
    $count = count($data);

    //We check all data -> if "statut" == "rendu" -> $statut = $statut - 1
    foreach ($data as $datum){
        if ($datum['statut'] == "Rendu"){
            $statut--;
        }
    }

    //If $statut is equal to 0 that means none is returned
    //If $statut is equal to (negativ) $count that means all snows are returned
    //If $statut is smaller than 0 that means only some snows are returned
    if ($statut == 0){
        $result = "En cours";
    }elseif ($statut == -$count){
        $result = "Rendu";
    }elseif ($statut < 0){
        $result = "Rendu partiel";
    }

    return $result;
}