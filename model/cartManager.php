<?php
/**
 * Author   : nicolas.glassey@cpnv.ch
 * Project  : 151_2019_code
 * Created  : 04.04.2019 - 18:48
 *
 * Last update :    [17.05.2019 arben.ferati@cpnv.ch]
 *                  [add : checkForErrors()]
 *                  [add : updateCartItem()]
 * Git source  :    [https://aslak_e36@bitbucket.org/aslak_e36/projetwebdb.git]
 */

/**
 * @param $currentCartArray -> The entire Cart
 * @param $snowCodeToAdd -> The code of the snow choosen
 * @param $qtyOfSnowsToAdd -> Number of snows to be added
 * @param $howManyLeasingDays -> Number of leasing days
 * @return array -> The cart updated
 */

function updateCart($currentCartArray, $snowCodeToAdd, $qtyOfSnowsToAdd, $howManyLeasingDays){
    $cartUpdated = array();
    $whatElementToAddUp = 0;
    $existsAlready = false;
    if($currentCartArray != null){
        $cartUpdated = $currentCartArray;
        foreach ($cartUpdated as $key => $item){
            if (($item['code']==$snowCodeToAdd)&&($item['nbD'] == $howManyLeasingDays)){
                $existsAlready = true;
                $whatElementToAddUp = $key;
            }
        }
        if ($existsAlready == true){
            $cartUpdated[$whatElementToAddUp]['qty'] += $qtyOfSnowsToAdd;
        }else {
            $newSnowLeasing = array('code' => $snowCodeToAdd, 'dateD' => Date("Y-m-d"), 'nbD' => $howManyLeasingDays, 'qty' => $qtyOfSnowsToAdd);
            array_push($cartUpdated, $newSnowLeasing);
        }
    }else {
        $newSnowLeasing = array('code' => $snowCodeToAdd, 'dateD' => Date("Y-m-d"), 'nbD' => $howManyLeasingDays, 'qty' => $qtyOfSnowsToAdd);
        array_push($cartUpdated, $newSnowLeasing);
    }


    return $cartUpdated;
}

//in_array https://www.php.net/manual/en/function.in-array.php
//array_push() https://www.php.net/manual/en/function.array-push.php
//array_search
//unset

/**
 * @param $snowsToAdd -> Number of snows to be added into the cart
 * @param $leasingDays -> Number of days wanted to leas the snow(s)
 * @param $snowCode -> The code of the snow that the user want to leas
 * @param $cart -> The cart, optional : When there's no cart we set it at null
 * @param null $index -> Index of the element to check
 * @return bool -> True if there's an error otherwise returns False
 */
function checkForErrors($snowsToAdd, $leasingDays, $snowCode, $cart = null, $index = null){
    $result = false;
    $newQty = $snowsToAdd;
    $q = "SELECT qtyAvailable FROM snows WHERE code='".$snowCode."'";

    require_once 'model/dbConnector.php';
    $qResult = executeQuerySelect($q);

    if ($index != null){
        foreach ($cart as $key => $item){
            if ($snowCode == $item['code'] and $key != $index){
                $newQty += $item['qty'];
            }
        }
        if (($newQty > $qResult[0][0]) || ($leasingDays < 1) || ($snowsToAdd < 1)){
            $result = true;
        }else {
            $result = false;
        }
    }else {
        if ($cart != null){
            foreach ($cart as $key => $item){
                if ($snowCode == $item['code']){
                    $newQty += $item['qty'];
                }
            }
            if (($newQty > $qResult[0][0]) || ($leasingDays < 1) || ($snowsToAdd < 1)){
                $result = true;
            }else {
                $result = false;
            }
        }else {
            if (($snowsToAdd > $qResult[0][0]) || ($leasingDays < 1) || ($snowsToAdd < 1)){
                $result = true;
            }else {
                $result = false;
            }
        }
    }
    return $result;
}

/**
 * @param $cart -> The current cart
 * @param $inputs -> New values
 * @param $index -> Index of item to change
 * @param null $delete -> [optional] If there's something to delete
 * @return array -> New cart
 */
function updateCartItem($cart, $inputs, $index, $delete = null){
    $newCart = array();
    if ($delete != null){
        unset($cart[$index]);
        $newCart = $cart;
        $delete = null;
    }else {
        $cart[$index]['qty'] = $inputs['uQty'.$index];
        $cart[$index]['nbD'] = $inputs['uNbD'.$index];
        $newCart = $cart;
    }
    return $newCart;
}