<?php
/**
 * This php file is designed to manage all operations regarding user's management
 * Author   : nicolas.glassey@cpnv.ch
 * Project  : Code
 * Created  : 31.01.2019 - 18:40
 *
 * Last update :    [01.12.2018 author]
 *                  [add $logName in function setFullPath]
 * Source       :   pascal.benzonana
 */

/**
 * This function is designed to verify user's login
 * @param $userEmailAddress
 * @param $userPsw
 * @return bool : "true" only if the user and psw match the database. In all other cases will be "false".
 */
function isLoginCorrect($userEmailAddress, $userPsw){
    $result = false;

    $strSeparator = '\'';
    $loginQuery = 'SELECT userHashPsw FROM users WHERE userEmailAddress = '. $strSeparator . $userEmailAddress . $strSeparator;

    require_once 'model/dbConnector.php';
    $queryResult = executeQuerySelect($loginQuery);

    if (count($queryResult) == 1)
    {
        $userHashPsw = $queryResult[0]['userHashPsw'];
        $hashPasswordDebug = password_hash($userPsw, PASSWORD_DEFAULT);
        $result = password_verify($userPsw, $userHashPsw);
    }
    return $result;
}

/**
 * This function is designed to register a new account
 * @param $userEmailAddress
 * @param $userPsw
 * @return bool|null
 */
function registerNewAccount($userEmailAddress, $userPsw){
    $result = false;

    $strSeparator = '\'';

    $userHashPsw = password_hash($userPsw, PASSWORD_DEFAULT);

    $registerQuery = 'INSERT INTO users (`userEmailAddress`, `userHashPsw`, `admin`) VALUES (' .$strSeparator . $userEmailAddress .$strSeparator . ','.$strSeparator . $userHashPsw .$strSeparator. ', 0)';

    require_once 'model/dbConnector.php';
    $queryResult = executeQueryInsert($registerQuery);
    if($queryResult){
        $result = $queryResult;
    }
    return $result;
}

/**
 * This function is designed to get the type of user
 * For the webapp, it will adapt the behavior of the GUI
 * @param $userEmailAddress
 * @return int (0 = customer ; 1 = seller)
 */
function getUserType($userEmailAddress){
    $result = 0;//we fix the result to 0 -> customer

    $strSeparator = '\'';

    $getUserTypeQuery = 'SELECT admin FROM users WHERE users.userEmailAddress =' . $strSeparator . $userEmailAddress . $strSeparator;

    require_once 'model/dbConnector.php';
    $queryResult = executeQuerySelect($getUserTypeQuery);

    if (count($queryResult) == 1){
        $result = $queryResult[0]['admin'];
    }
    return $result;
}

/**
 * @param $user -> The user email address to identify him when we do SQL requests
 * @return bool -> True or False depending if the user has rents or not
 */
function hasUserRents($user){
    require_once 'model/dbConnector.php';

    $result = false;

    $q = 'SELECT id FROM users where userEmailAddress = "' . $user . '"';
    $userId = executeQuerySelect($q);

    $q = 'select * from rentDetails where users_id ="' . $userId[0][0] . '"';
    $rentDetails = executeQuerySelect($q);

    if (count($rentDetails) <= 0){
        return false;
    }else {
        return true;
    }
}