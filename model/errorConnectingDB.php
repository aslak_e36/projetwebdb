<?php
/**
 * Thi is the class to manage errors with database
 * Author   : arben.ferati@cpnv.ch
 * Project  : Connecting errors management
 * Created  : 12.06.2019
 */


class errorConnectingDB extends Exception {
    public $msgGui = "Notre site est en maintenance, merci pour votre compréhension";
    public $msg = "Error connecting with database";
}