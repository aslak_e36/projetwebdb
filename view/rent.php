<?php
/**
 * This php file is designed to finalize the shopping cart
 * Author   : nicolas.kaelin@cpnv.ch
 * Project  : Code
 * Created  : 20.05.2019 - 21:40
 *
 * Last update :    [20.05.2019 NK]
 *                  []
 * Source       :   pascal.benzonana
 */



$title = 'Rent A Snow - Demande de location';

ob_start();
?>
<?php if(isset($_GET['error']['cart']) && $_GET['error']['cart'] == true): ?>
    <div class="alert alert-danger"><strong>Erreur sur la quantité demandée.</strong>
        <br>
        Quantité trop élevée ou inférieure à 1. Vérifiez le disponibilité du stock.
    </div>
<?php endif; ?>
    <h2>Vos Locations</h2>
    <div>
        Votre demande de location a été enregistrée.
    <br>
        Si vous voulez visualiser votre demande, cliquez sur le logo <a href="">PDF</a>
    </div>
    <article>
        <form id="cartForm" name="cartForm" method="post" action="">
            <table class="table">
                <tr>
                    <th>N° Location</th>
                    <th>Code</th>
                    <th>Marque</th>
                    <th>Modèle</th>
                    <th>Prix</th>
                    <th>Quantité</th>
                    <th>Date début de location</th>
                </tr>
                <?php foreach ($cartArray as $key => $article): ?>
                    <tr>
                        <td><?= $article['nLocation'] ?></td>
                        <td><?= $article['code'] ?></td>
                        <td><?= $article['brand'] ?></td>
                        <td><?= $article['model'] ?></td>
                        <td><?= $article['dailyPrice'] ?></td>
                        <td><?= $article['qtyAvailable'] ?></td>
                        <td><?= $article['dateD'] ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </form>
    </article>

<?php
$content = ob_get_clean();
require 'gabarit.php';
?>
