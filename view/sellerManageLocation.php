<?php

$title="Single rent management";
// Tampon de flux stocké en mémoire
ob_start();
?>

    <?php $selectIndex = 0 ?>

    <h4>Gestion des retours</h4>

    <div class="row-fluid">
        <div class="span2">
            <strong>Location</strong>: <?= $rentId ?> <br>
            <strong>Prise</strong>: <?= $cartArray[0]['firstDate'] ?> <br>
            <strong>Status</strong>: <?= $statut ?>
        </div>
        <div class="span2">
            <strong>Email</strong>: <?= $cartArray[0]['email'] ?>
            <strong>Retour</strong>: <?= $cartArray[0]['lastDate'] ?>
        </div>
    </div>
    <br>
    <form id="cartForm" name="cartForm" method="post" action="index.php?action=manageRents&rentId=<?= $rentId ?>">
        <table class="table">
            <tr>
                <th>Code</th>
                <th>Quantité</th>
                <th>Prise</th>
                <th>Retour</th>
                <th>Statut</th>
            </tr>
            <?php foreach ($cartArray as $key => $article): ?>
                <tr>
                    <td><?= $article['code'] ?></td>
                    <td><?= $article['qty'] ?></td>
                    <td><?= $article['firstDate'] ?></td>
                    <td><?= $article['lastDate'] ?></td>
                    <td>
                        <?php if ($article['statut']=="En cours"): ?>
                            <select name="<?= $selectIndex ?>" id="status">
                                    <option name="<?= $article['statut'] ?>" value="<?= $article['statut'] ?>"><?= $article['statut'] ?></option>
                                    <option name="Rendu" value="Rendu">Rendu</option>
                            </select>
                        <?php $selectIndex++ ?>
                        <?php else: ?>
                            <p>Rendu</p>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
        <div class="span8">
            <a href="index.php?action=displaySellerOverview" class="btn">Retour à la vue d'ensemble</a>
        </div>
        <?php
            $statut = 0;
            foreach ($cartArray as $key => $cart){
                if ($cart['statut'] == "Rendu"){
                    $statut++;
                }else{
                    $statut--;
                }
            }
        ?>
        <?php if ($statut < count($cartArray) and $statut >= 0 or $statut < 0): ?>
                <div class="span3">
                    <button class="btn btn-success" type="submit">Enregistrer les modifications</button>
                </div>
        <?php endif; ?>
    </form>
<?php
$content = ob_get_clean();
require 'gabarit.php';
?>