<?php
/**
 * This php file is designed to manage all operation regarding cart's management
 * Author   : pascal.benzonana@cpnv.ch
 * Project  : Code
 * Created  : 23.03.2019 - 21:40
 *
 * Last update :    [24.03.2019 PBA]
 *                  []
 * Source       :   pascal.benzonana
 */



$title = 'Rent A Snow - Demande de location';

ob_start();
?>
    <?php if(isset($_GET['error']['cart']) && $_GET['error']['cart'] == true): ?>
        <div class="alert alert-danger"><strong>Erreur sur la quantité demandée.</strong>
            <br>
            Quantité trop élevée ou inférieure à 1. Vérifiez le disponibilité du stock.
        </div>
    <?php endif; ?>
    <h2>Votre Panier</h2>
    <article>
        <form id="cartForm" name="cartForm" method="post" action="">
            <table class="table">
                <tr>
                    <th>Code</th>
                    <th>Date</th>
                    <th>Quantité</th>
                    <th>Nombre de jours</th>
                    <th>Retirer / Mettre à jour</th>
                </tr>
                <?php $cartArray = $_SESSION['cart'] ?>
                <?php foreach ($cartArray as $key => $article): ?>
                    <tr>
                        <td><?= $article['code'] ?></td>
                        <td><?= $article['dateD'] ?></td>
                        <td><input type="number" name="uQty<?= $key ?>" value="<?= $article['qty'] ?>"></td>
                        <td><input type="number" name="uNbD<?= $key ?>" value="<?= $article['nbD'] ?>"></td>
                        <td>
                            <a href="index.php?action=updateCartRequest&code=<?= $article['code'] ?>&delete=<?= $key ?>"><img src='view/content/images/delete2.png'></a>
                            <input type="submit" value="Changer" class="btn btn-primary" onclick="cartForm.action='index.php?action=updateCartRequest&code=<?= $article['code'] ?>&update=<?= $key ?>'">
                        </td>
                        <td></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </form>
        <form id="submitForm" name="submitForm" method="post" action="">
            <input type="submit" value="Louer encore" class="btn btn-primary" name="backToCatalog" onclick="submitForm.action='index.php?action=displaySnows'">
            <?php if(isset($_SESSION['cart']) and (count($_SESSION['cart']) != 0)) :?>
                <input type="submit" value="Vider le panier" class="btn btn-cancel" name="resetCart" onclick="submitForm.action='index.php?action=displaySnows'">
                <input type="submit" value="Finaliser la location" class="btn btn-success" name="backToCatalog" onclick="submitForm.action='index.php?action=fixRent'">
            <?php endif; ?>
        </form>
    </article>

<?php
$content = ob_get_clean();
require 'gabarit.php';
?>