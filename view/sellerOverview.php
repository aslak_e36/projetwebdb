<?php

$title="Rents management";
// Tampon de flux stocké en mémoire
ob_start();
?>

    <?php
        $statut = 0;
        foreach ($cartArray as $key => $cart){
            if ($cart['statut'] == "Rendu"){
                $statut++;
            }elseif ($cart['statut'] == "En cours"){
                $statut--;
            }
        }
    ?>

    <h4>Location en cours</h4>
    <form id="cartForm" name="cartForm" method="post" action="">
        <table class="table">
            <tr>
                <th>Location</th>
                <th>Client</th>
                <th>Prise</th>
                <th>Retour</th>
                <th>Statut</th>
            </tr>
            <?php foreach ($cartArray as $key => $article): ?>
                <tr>
                    <td><a href="index.php?action=displaySellerManageLocation&rentId=<?= $article['location'] ?>"><?= $article['location'] ?></a></td>
                    <td><?= $article['client'] ?></td>
                    <td><?= $article['firstDate'] ?></td>
                    <td><?= $article['lastDate'] ?></td>
                    <td><?= $article['statut'] ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </form>
<?php
$content = ob_get_clean();
require 'gabarit.php';
?>
