<?php
/** Created by PhpStorm
 * User : Ricardo
 * Date : 24.05.2019
 * Time : 11:10
 * Project : projetwebdb
 */

/**
 * This function is designed to display Snows
 * There are two different view available.
 * One for the seller, an other one for the customer.
 */
function displaySnows(){
    require_once "model/fileManager.php";
    require_once "model/errorConnectingDB.php";
    try{
        if (isset($_POST['resetCart'])) {
            unset($_SESSION['cart']);
        }
        if (isset($_POST['changeCart'])) {
            $_SESSION['code'];
            $_POST['uQty'];
            $_POST['uNbD'];
        }
        require_once "model/snowsManager.php";
        $snowsResults = getSnows();

        $_GET['action'] = "displaySnows";
        if (isset($_SESSION['userType']))
        {
            switch ($_SESSION['userType']) {
                case 0://this is a customer
                    require "view/snows.php";
                    break;
                case 1://this a seller
                    require "view/snowsSeller.php";
                    break;
                default:
                    require "view/snows.php";
                    break;
            }
        }else{
            require "view/snows.php";
        }
    }catch (errorConnectingDB $exception){
        $errorConnecting = $exception->msgGui;
        errorLog($exception->msg);
        home($errorConnecting);
    }
}

/**
 * This function is designed to get only one snow results (for aSnow view)
 * @param $snow_code
 */
function displayASnow($snow_code){
    require_once "model/fileManager.php";
    require_once "model/errorConnectingDB.php";
    try{
        if (isset($registerRequest['inputUserEmailAddress'])){
            //TODO
        }
        require_once "model/snowsManager.php";
        $snowsResults= getASnow($snow_code);
        require "view/aSnow.php";
    }catch (errorConnectingDB $exception){
        $errorConnecting = $exception->msgGui;
        errorLog($exception->msg);
        home($errorConnecting);
    }
}