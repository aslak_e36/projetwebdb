<?php
/** Created by PhpStorm
 * User : Ricardo
 * Date : 24.05.2019
 * Time : 11:10
 * Project : projetwebdb
 */

/**
 *  This function is designed to create a location
 */
function fixRent(){
    require_once "model/fileManager.php";
    require_once "model/errorConnectingDB.php";
    try{
        if (isset($_SESSION['userEmailAddress'])){
            require_once "model/rentManager.php";
            $_GET['action'] = "fixRent";
            $cartArray = finishRent($_SESSION['cart'], $_SESSION['userEmailAddress']);
            unset($_SESSION['cart']);
            $_SESSION['rent'] = true;
            require "view/rent.php";
        }else{
            require "view/login.php";
        }
    }catch (errorConnectingDB $exception){
        $errorConnecting = $exception->msgGui;
        errorLog($exception->msg);
        home($errorConnecting);
    }
}

/**
 * This function is designed to display all the rents of the logged user
 */
function displayRent(){
    require_once "model/fileManager.php";
    require_once "model/errorConnectingDB.php";
    try{
        require_once "model/rentManager.php";
        $_GET['action'] = "displayRent";
        $cartArray = getAllRentsFromAUser($_SESSION['userEmailAddress']);
        require "view/rent.php";
    }catch (errorConnectingDB $exception){
        $errorConnecting = $exception->msgGui;
        errorLog($exception->msg);
        home($errorConnecting);
    }
}

/**
 * This function is designed to display the sellerOverview view for the admin users
 */
function displaySellerOverview(){
    if (isset($_SESSION['userType']) and $_SESSION['userType'] == 1){
        require_once "model/fileManager.php";
        require_once "model/errorConnectingDB.php";
        try{
            require_once "model/rentManager.php";
            $_GET['action'] = "displaySellerOverview";
            $cartArray = getAllRents();
            require "view/sellerOverview.php";
        }catch (errorConnectingDB $exception){
            $errorConnecting = $exception->msgGui;
            errorLog($exception->msg);
            home($errorConnecting);
        }
    }else{
        home();
    }
}

/**
 * This function is designed to display the sellerManageLocation view for the admin users
 * @param $rentId
 */
function displaySellerManageLocation($rentId){
    if (isset($_SESSION['userType']) and $_SESSION['userType'] == 1){
        require_once "model/fileManager.php";
        require_once "model/errorConnectingDB.php";
        try{
            require_once "model/rentManager.php";
            $_GET['action'] = "displaySellerManageLocation";
            $cartArray = displayRents($rentId);
            $statut = checkStatut($cartArray);
            require "view/sellerManageLocation.php";
        }catch (errorConnectingDB $exception){
            $errorConnecting = $exception->msgGui;
            errorLog($exception->msg);
            home($errorConnecting);
        }
    }else{
        home();
    }
}

/**
 * This function is desigend to update the the rents who are returned from users
 * @param $rentId
 * @param $data
 */
function manageRents($rentId, $data){
    require_once "model/fileManager.php";
    require_once "model/errorConnectingDB.php";
    try{
        require_once "model/rentManager.php";
        $_GET['action'] = "manageRents";
        $cartArray = updateRents($rentId, $data);
        $statut = checkStatut($cartArray);
        require "view/sellerManageLocation.php";
    }catch (errorConnectingDB $exception){
        $errorConnecting = $exception->msgGui;
        errorLog($exception->msg);
        home($errorConnecting);
    }
}