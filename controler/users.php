<?php
/** Created by PhpStorm
 * User : Ricardo
 * Date : 24.05.2019
 * Time : 11:10
 * Project : projetwebdb
 */

/**
 * This function is designed to redirect the user to the home page (depending on the action received by the index)
 */
function home($errorConnecting = null){
    require_once "model/fileManager.php";
    $_GET['action'] = "home";
    require "view/home.php";
}

/**
 * This function is designed to manage login request
 * @param $loginRequest containing login fields required to authenticate the user
 */
function login($loginRequest){
    require_once "model/fileManager.php";
    require_once "model/errorConnectingDB.php";
    //if a login request was submitted
    try{
        if (isset($loginRequest['inputUserEmailAddress']) && isset($loginRequest['inputUserPsw'])) {
            //extract login parameters
            $userEmailAddress = $loginRequest['inputUserEmailAddress'];
            $userPsw = $loginRequest['inputUserPsw'];

            //try to check if user/psw are matching with the database
            require_once "model/usersManager.php";
            if (isLoginCorrect($userEmailAddress, $userPsw)) {
                createSession($userEmailAddress);
                $_GET['loginError'] = false;
                $_SESSION['rent'] = hasUserRents($loginRequest['inputUserEmailAddress']);
                $_GET['action'] = "home";
                require "view/home.php";
            } else { //if the user/psw does not match, login form appears again
                $_GET['loginError'] = true;
                $_GET['action'] = "login";
                require "view/login.php";
            }
        }else{ //the user does not yet fill the form
            $_GET['action'] = "login";
            require "view/login.php";
        }
    }catch (errorConnectingDB $exception){
        $errorConnecting = $exception->msgGui;
        errorLog($exception->msg);
        home($errorConnecting);
    }
}

/**
 * This fonction is designed to register a new user
 * @param $registerRequest
 */
function register($registerRequest){
    require_once "model/fileManager.php";
    require_once "model/errorConnectingDB.php";
    //variable set
    try{
        if (isset($registerRequest['inputUserEmailAddress']) && isset($registerRequest['inputUserPsw']) && isset($registerRequest['inputUserPswRepeat'])) {

            //extract register parameters
            $userEmailAddress = $registerRequest['inputUserEmailAddress'];
            $userPsw = $registerRequest['inputUserPsw'];
            $userPswRepeat = $registerRequest['inputUserPswRepeat'];

            if ($userPsw == $userPswRepeat){
                require_once "model/usersManager.php";
                if (registerNewAccount($userEmailAddress, $userPsw)){
                    createSession($userEmailAddress);
                    $_GET['registerError'] = false;
                    $_GET['action'] = "home";
                    require "view/home.php";
                }
            }else{
                $_GET['registerError'] = true;
                $_GET['action'] = "register";
                require "view/register.php";
            }
        }else{
            $_GET['action'] = "register";
            require "view/register.php";
        }
    }catch (errorConnectingDB $exception){
        $errorConnecting = $exception->msgGui;
        errorLog($exception->msg);
        home($errorConnecting);
    }
}

/**
 * This function is designed to create a new user session
 * @param $userEmailAddress : user unique id
 */
function createSession($userEmailAddress){
    require_once "model/fileManager.php";
    require_once "model/errorConnectingDB.php";
    try{
        $_SESSION['userEmailAddress'] = $userEmailAddress;
        //set user type in Session
        $userType = getUserType($userEmailAddress);
        $_SESSION['userType'] = $userType;
    }catch (errorConnectingDB $exception){
        $errorConnecting = $exception->msgGui;
        errorLog($exception->msg);
        home($errorConnecting);
    }
}

/**
 * This function is designed to manage logout request
 */
function logout(){
    require_once "model/fileManager.php";
    require_once "model/errorConnectingDB.php";
    try{
        $_SESSION = array();
        session_destroy();
        $_GET['action'] = "home";
        require "view/home.php";
    }catch (errorConnectingDB $exception){
        $errorConnecting = $exception->msgGui;
        errorLog($exception->msg);
        home($errorConnecting);
    }
}