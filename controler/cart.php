<?php
/** Created by PhpStorm
 * User : Ricardo
 * Date : 24.05.2019
 * Time : 11:11
 * Project : projetwebdb
 */

/**
 * This function is designed to display a single cart
 */
function displayCart(){
    $_GET['action'] = "cart";
    require "view/cart.php";
}

/**
 * This function displays the snowLeasingRequest view
 * @param $snowCode
 */
function snowLeasingRequest($snowCode){
    require_once "model/errorConnectingDB.php";
    try{
        require_once "model/snowsManager.php";
        $snowsResults = getASnow($snowCode);
        $_GET['action'] = "snowLeasingRequest";
        require "view/snowLeasingRequest.php";
    }catch (errorConnectingDB $exception){
        $errorConnecting = $exception->msgGui;
        errorLog($exception->msg);
        home($errorConnecting);
    }
}

/**
 * This function designed to manage all request impacting the cart content
 * @param $snowCode -> code of the specific snow
 * @param $snowLocationRequest -> it contains $_POST['uQty'] (is the quantity of snows rented) and $_POST['uNbD'] (is the quantity of days to rent)
 * @param null $update -> is set in case the program update something
 * @param null $delete -> is set in case the program delete something
 */
function updateCartRequest($snowCode, $snowLocationRequest, $update = null, $delete = null){
    require_once "model/fileManager.php";
    require_once "model/errorConnectingDB.php";
    require_once "model/cartManager.php";
    require_once "model/snowsManager.php";
    try{
        if ($update!=null){
            if (checkForErrors($snowLocationRequest['uQty'.$update], $snowLocationRequest['uNbD'.$update], $snowCode, $_SESSION['cart'], $update)){
                $_GET['error']['cart'] = true;
                $_GET['action'] = "displayCart";
                displayCart();
            }else{
                $_GET['error']['cart'] = false;
                $cartArrayTemp = updateCartItem($_SESSION['cart'], $_POST, $_GET['update']);
                $_SESSION['cart'] = $cartArrayTemp;
                $_GET['action'] = "displayCart";
                displayCart();
            }
        }elseif ($delete!=null){
            $cartArrayTemp = updateCartItem($_SESSION['cart'], $_POST, $_GET['delete'], 1);
            $_SESSION['cart'] = $cartArrayTemp;
            $_GET['action'] = "displayCart";
            displayCart();
        }else{
            if (checkForErrors($snowLocationRequest['inputQuantity'],$snowLocationRequest['inputDays'], $snowCode, $_SESSION['cart']??null)){
                $_GET['error']['cart'] = true;
                snowLeasingRequest($snowCode);
            }else{
                $_GET['error']['cart'] = false;
                $cartArrayTemp = array();
                if(isset($snowLocationRequest) AND isset($snowCode)) {
                    if (isset($_SESSION['cart'])) {
                        $cartArrayTemp = $_SESSION['cart'];
                    }
                    $cartArrayTemp = updateCart($cartArrayTemp, $snowCode, $snowLocationRequest['inputQuantity'], $snowLocationRequest['inputDays']);
                    $_SESSION['cart'] = $cartArrayTemp;
                }
                $_GET['action'] = "displayCart";
                displayCart();
            }
        }
    }catch (errorConnectingDB $exception){
        $errorConnecting = $exception->msgGui;
        errorLog($exception->msg);
        home($errorConnecting);
    }
}